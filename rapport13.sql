SELECT ticket_id "Numéro du billet", first_name "Prénom", last_name "Nom", train_id "Numéro de train"
FROM t_customer cus
    JOIN t_reservation res
        ON cus.customer_id = res.buyer_id
    JOIN t_ticket tic
        ON res.reservation_id = tic.reservation_id
    JOIN t_wagon_train wagt
        ON tic.wag_tr_id = wagt.wag_tr_id
    JOIN t_train tra
        ON wagt.train_id = tra.train_id
    JOIN t_wagon wag
        ON wagt.wagon_id = wag.wagon_id
    JOIN t_pass pass
        ON cus.pass_id = pass.pass_id
WHERE wag.class_type = 1 AND res.price IS NOT NULL AND pass.pass_name LIKE '15-25' AND tra.departure_time BETWEEN '15/04/2019' AND '25/04/2019'
ORDER BY res.creation_date;