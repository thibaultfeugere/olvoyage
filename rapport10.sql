SELECT COUNT(cus.customer_id) "Nombre de voyageurs"
FROM t_pass pass
JOIN t_customer cus
ON pass.pass_id = cus.pass_id
JOIN t_reservation res
ON cus.customer_id = res.buyer_id
JOIN t_ticket tic
ON res.reservation_id = tic.reservation_id
JOIN t_wagon_train wag
ON tic.wag_tr_id = wag.wag_tr_id
JOIN t_train tra
ON wag.train_id = tra.train_id
WHERE UPPER(pass.pass_name) LIKE 'SENIOR' AND tra.departure_time LIKE '%/03/2019' AND res.price IS NOT NULL;