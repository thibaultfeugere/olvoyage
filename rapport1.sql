SELECT last_name || ' ' || first_name "Employé", COUNT(reservation_id) "Nombre de réservation"
FROM t_employee e
JOIN t_reservation r
ON (e.employee_id = r.employee_id)
GROUP BY last_name, first_name
HAVING COUNT(reservation_id) = (SELECT MAX(COUNT(reservation_id)) 
								FROM t_reservation 
								GROUP BY employee_id);