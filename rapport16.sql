SELECT employee_id "N° de l'employé", last_name "NOM", first_name "PRÉNOM", salary "SALAIRE", salary + 100 "Nouveau_salaire"
FROM t_employee
WHERE employee_id IN  (SELECT employee_id FROM t_employee WHERE manager_id = 1);