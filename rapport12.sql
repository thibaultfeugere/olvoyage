SELECT COUNT(p.pass_name) "Nombre de ventes", p.pass_name "Type Abonnement"
FROM t_pass p
JOIN t_customer c
ON (c.pass_id = p.pass_id)
GROUP BY pass_name
ORDER BY count(p.pass_name) DESC;