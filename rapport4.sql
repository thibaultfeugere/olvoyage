SELECT c.last_name "Nom", c.first_name "Pr�nom", p.pass_name "Abonnement",
CASE 
	WHEN MONTHS_BETWEEN('15/05/19', c.pass_date) >= 12 
        THEN 'Perim� !'
	WHEN c.pass_date IS NULL 
        THEN 'Aucun'
    ELSE 'En cours...'
END "Abonnement"
FROM t_customer c
JOIN t_pass p 
ON (c.pass_id = p.pass_id)
ORDER BY c.last_name, c.first_name;