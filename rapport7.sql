SELECT *
FROM  (SELECT t.train_id "Num train", dep.city ||' - '|| arv.city "Top 5"
		FROM t_train t
			JOIN t_station dep ON t.departure_station_id = dep.station_id
			JOIN t_station arv ON t.arrival_station_id = arv.station_id
			JOIN t_wagon_train wt ON t.train_id = wt.train_id
			JOIN t_ticket t ON wt.wag_tr_id = t.wag_tr_id
		GROUP BY t.train_id, dep.city ||' - '|| arv.city
		ORDER BY COUNT(t.ticket_id) DESC)
WHERE ROWNUM <=5 
/