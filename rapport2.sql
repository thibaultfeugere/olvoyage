SELECT DISTINCT cus.last_name "Nom", cus.first_name "Prénom" 
FROM t_reservation res
JOIN t_customer cus
ON cus.customer_id = res.buyer_id
JOIN t_ticket tic
ON res.buyer_id != tic.customer_id
ORDER BY last_name, first_name;