SELECT t.train_id "N° de train", s.city "Ville", t.departure_time "Heure de départ", s.city AS "Départ", st.city AS "Arrivée", 
t.arrival_time "Heure d'arrivée", t.distance "Distance", t.price "Prix"
FROM t_train t
JOIN t_station s 
ON (t.departure_station_id = s.station_id)
JOIN t_station st 
ON (t.arrival_station_id = st.station_id)
ORDER BY train_id;