SELECT r.reservation_id "Numéro de réservation", r.creation_date "Date de création", 
e.last_name ||' '|| e.first_name "Employé", 
c.last_name||' '|| c.first_name "Client "
FROM t_reservation r 
JOIN t_customer c
ON (c.customer_id = r.buyer_id)
JOIN t_employee e
ON (r.employee_id = e.employee_id)
WHERE r.creation_date = (SELECT MIN(r.creation_date) FROM t_reservation r) ;