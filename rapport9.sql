SELECT dep.city || ' - ' || arv.city "Nom du train", 
ROUND((distance/((arrival_time-departure_time)*(60*24)))*60, 0) || ' km/h' "Vitesse"
FROM t_train t
JOIN t_station dep
ON (t.departure_station_id = dep.station_id)
JOIN t_station arv
ON (t.arrival_station_id = arv.station_id)
ORDER BY train_id
/